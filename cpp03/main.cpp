#include <iostream>
#include <vector>

using namespace std;

#define LOG_FUNCTION() std::cout<<  __PRETTY_FUNCTION__ << std::endl;

class Object
{
  public:
    Object()
    {
        LOG_FUNCTION();
    }

    Object(const Object&)
    {
        LOG_FUNCTION();
    }

    void doSomething()
    {
        LOG_FUNCTION();
    }

    void doSomethingElse()
    {
        LOG_FUNCTION();
    }

//    ~Object()
//    {
//        LOG_FUNCTION();
//    }
};


vector<Object> makeBigVector() {
   vector<Object> result(5);
   return result;
}


int main()
{
    vector<Object> v = makeBigVector();
    cout << "Hello World!" << endl;
    return 0;
}
