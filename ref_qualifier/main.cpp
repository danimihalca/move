#include <iostream>

using namespace std;

#define LOG_FUNCTION() std::cout<<  __PRETTY_FUNCTION__ << std::endl;

class Object
{
  public:
    Object()
    {
        LOG_FUNCTION();
    }

    Object(const Object&)
    {
        LOG_FUNCTION();
    }

    Object(Object&&)
    {
        LOG_FUNCTION();
    }

    void doSomething()
    {
        LOG_FUNCTION();
    }

    ~Object()
    {
        LOG_FUNCTION();
    }
};

class Wrapper
{
  public:
    Wrapper()
    {
        LOG_FUNCTION();
    }

    Wrapper(const Wrapper&)
    {
        LOG_FUNCTION();
    }

    const Object& getObject() const
    {
        LOG_FUNCTION();
        return m_object;
    }

    /*
    const Object& getObject() const &
    {
        LOG_FUNCTION();
        return m_object;
    }

    Object&& getObject() &&
    {
        LOG_FUNCTION();
        return std::move(m_object);
    }
*/
private:
    Object m_object;
};



int main()
{
    auto o = Wrapper().getObject();
    o.doSomething();

    return 0;
}


