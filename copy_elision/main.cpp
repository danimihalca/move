#include <iostream>
#include <cstdlib>
#include <ctime>

#define LOG_FUNCTION() std::cout<<  __PRETTY_FUNCTION__ << std::endl;

class Object
{
  public:
    Object()
    {
        LOG_FUNCTION();
    }

    Object(const Object&)
    {
        LOG_FUNCTION();
    }

    void doSomething()
    {
        LOG_FUNCTION();
    }

    void doSomethingElse()
    {
        LOG_FUNCTION();
    }

//    ~Object()
//    {
//        LOG_FUNCTION();
//    }
};


//RVO
Object createObject()
{
    return Object{};
}

//Non-NRVO
Object createObject2(bool b)
{
    if (b)
    {
        Object x1;
        x1.doSomething();
        return x1;
    }
    else
    {
        Object x2;
        x2.doSomethingElse();
        return x2;
    }
}

//NRVO
Object createObject3(bool b)
{
    Object x;
    if (b)
    {
        x.doSomething();
    }
    else
    {
        x.doSomethingElse();
    }
    return x;
}



int main()
{
    std::cout << "Creating o1" << std::endl;
    Object o1 = Object(Object(Object()));

    std::cout << "Creating o2" << std::endl;
    Object o2 = createObject();

    srand (time(NULL));

    std::cout << "Creating o3" << std::endl;
    Object o3 = createObject2(rand() % 2);

    std::cout << "Creating o4" << std::endl;
    Object o4 = createObject3(rand() % 2);
    return 0;
}
