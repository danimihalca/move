#include <iostream>

#define LOG_FUNCTION() std::cout<<  __PRETTY_FUNCTION__ << std::endl;
#define LOG_OBJECT_METHOD() std::cout << "Object[" << _id << "]: " <<  __PRETTY_FUNCTION__ << std::endl;

class Object
{
  public:
    Object(int id)
        :_id{id}
    {
       LOG_OBJECT_METHOD();
    }

    Object(const Object&)
    {
        LOG_OBJECT_METHOD();
    }

    Object(Object&&)
    {
        LOG_OBJECT_METHOD();
    }

private:
    int _id;
};

Object createObject(int x)
{
    auto o = Object{x};
    return o;
}

Object doSomething (const Object&, const Object&)
{
    LOG_FUNCTION();
    return {0};
}

Object doSomething (Object&&, Object&&)
{
    LOG_FUNCTION();
    return {0};
}

int main()
{
//    auto o = doSomething(createObject(10), createObject(1));
    auto ao = (doSomething(createObject(10), createObject(1)));

    return 0;
}
