#include <iostream>

#define LOG_FUNCTION() std::cout<<  __PRETTY_FUNCTION__ << std::endl;

class Object
{
  public:
    Object()
    {
        LOG_FUNCTION();
    }

    Object(const Object&)
    {
        LOG_FUNCTION();
    }

    Object(Object&&)
    {
        LOG_FUNCTION();
    }
};


//RVO
Object createObject()
{
    return Object{};
}

//NRVO
Object createNamedObject()
{
    Object x{};
    return x;
}


int main()
{
    std::cout << "Creating o1" << std::endl;
    Object o1 = Object(Object(Object()));

    std::cout << "Creating o2" << std::endl;
    Object o2 = createObject();

    std::cout << "Creating o3" << std::endl;
    Object o3 = createNamedObject();
    return 0;
}
