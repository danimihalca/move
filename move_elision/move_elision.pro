TEMPLATE = app
CONFIG += console
CONFIG -= c++11 app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -fno-elide-constructors
#QMAKE_CXXFLAGS += -std=c++98
SOURCES += main.cpp
