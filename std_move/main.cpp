#include <iostream>

using namespace std;

#define LOG_FUNCTION() std::cout<<  __PRETTY_FUNCTION__ << std::endl;

class Object
{
  public:
    Object()
    {
        LOG_FUNCTION();
    }

    Object(const Object&)
    {
        LOG_FUNCTION();
    }

    Object& operator=(const Object&)
    {
        LOG_FUNCTION();

        return *this;
    }

    Object(Object&&)
    {
        LOG_FUNCTION();
    }

    Object& operator=(Object&&)
    {
        LOG_FUNCTION();

        return *this;
    }

    ~Object()
    {
        LOG_FUNCTION();
    }
};

class Wrapper
{
  public:
    Wrapper()
    {
        LOG_FUNCTION();
    }

    Wrapper(const Wrapper&)
    {
        LOG_FUNCTION();
    }

    void setObject(const Object& aObject)
    {
        LOG_FUNCTION();
        m_object = aObject;
    }

    void setObject(Object&& aObject)
    {
        LOG_FUNCTION();
        m_object = std::move(aObject);
//        m_object = aObject;
    }
private:
    Object m_object;
};


int main()
{
    Wrapper w;
    w.setObject(Object{});

    return 0;
}


